# Steps to run this AsgardCms App

1. Git Clone and Change Directory to this Repo: <br />
`git clone https://gitlab.com/bidd98/asgardcms-test.git && cd asgardcms-test`

2. Start Containers: <br />
`docker-compose up`

3. Get into our app bash shell: <br />
`docker-compose exec app sh` <br />
UI: <br />
`/var/www/app #`

4. Install needed dependencies for our app: <br />
`/var/www/app # composer install`

5. Initialize Asgard CMS <br />
`/var/www/app # php artisan asgard:install`
```
Enter your database driver (e.g. mysql, pgsql) [mysql]:
 > mysql
 Enter your database host [127.0.0.1]:
 > db
 Enter your database port [3306]:
 > 3306
 Enter your database name [homestead]:
 > test_db
 Enter your database username [homestead]:
 > bidd
 Enter your database password (leave <none> for no password) [secret]:
 > root
 Enter you application url (e.g. http://localhost, http://dev.example.com) [http://localhost]:
 > http://localhost
 Enter your first name:
 > hung
 Enter your last name:
 > nguyen
 Enter your email address:
 > test@gmail.com
 Enter a password:
 > test
 Please confirm your password:
 > test
```

6. We need to grant permission on folder storage/ and bootstrap/cache <br />
__Note:__ Never set these directories to 777, best practice is to assign group permission to them <br />
`/var/www/app # chown -R $USER:nginx storage` <br />
`/var/www/app # chown -R $USER:nginx bootstrap/cache` <br />
`/var/www/app # chmod -R 775 storage` <br />
`/var/www/app # chmod -R 775 bootstrap/cache` <br />

7. Generate Encryption key<br />
`/var/www/app # php artisan key:generate`

8. Create symbolic link to local storage for storing images <br />
`/var/www/app # php artisan storage:link`

9. Run migration <br />
`/var/www/app # php artisan migrate` <br />

10. Enjoy our app at: <br />
[Click here to go to your admin panel](http://localhost:11400/en/backend)<br />
login with credential:
- email: test@gmail.com
- password: test

11. After login, go to Workshop -> Users -> Roles -> Admin -> Permissions -> Allow all Product.products -> save <br />

12. And now you can manage your Product Module in the Admin Panel

13. Product API: <br />
`http://localhost:11400/en/api/product/products?page={page number here}`
