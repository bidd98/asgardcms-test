<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Response;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;
use Modules\Product\Repositories\ProductRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends AdminBaseController
{
    /**
     * @var ProductRepository
     */
    private $product;

    public function __construct(ProductRepository $product)
    {
        parent::__construct();

        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = $this->product->all();

        return view('product::admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product::admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $data = $request->validated();

        $filePath = $this->storeImage($data['image']);

        $this->product->create([
            'name' => $data['name'],
            'price' => $data['price'],
            'description' => $data['description'],
            'image_path' => '/storage/' . $filePath,
        ]);

        return redirect()->route('admin.product.product.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::products.title.products')]));
    }

    /**
     * Store image to storage
     * @return string
     */
    private function storeImage($image)
    {
        $filePath = $image->store('images/product', 'public');
        $image = Image::make(public_path('storage/' . $filePath))->resize(300, 300);
        $image->save();
        return $filePath;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return Response
     */
    public function edit(Product $product)
    {
        return view('product::admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Product $product
     * @param  UpdateProductRequest $request
     * @return Response
     */
    public function update(Product $product, UpdateProductRequest $request)
    {
        $data = $request->validated();

        // Check if $data contain image
        if (array_key_exists('image', $data)) {
            $filePath = $this->storeImage($data['image']);
            $this->product->update($product, [
                'name' => $data['name'],
                'price' => $data['price'],
                'description' => $data['description'],
                'image_path' => '/storage/' . $filePath,
            ]);
        } else {
            $this->product->update($product, $request->all());
        }

        return redirect()->route('admin.product.product.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::products.title.products')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        $this->product->destroy($product);

        return redirect()->route('admin.product.product.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::products.title.products')]));
    }

    /**
     * Return Product Json API
     * @return JSON
     */
    public function apiIndex()
    {
        $products = $this->product->all();

        return $products;
    }
}
