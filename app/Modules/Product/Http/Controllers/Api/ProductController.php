<?php

namespace Modules\Product\Http\Controllers\Api;

use Modules\Product\Repositories\ProductRepository;

class ProductController
{
    /**
     * @var ProductRepository
     */
    private $product;

    public function __construct(ProductRepository $product)
    {

        $this->product = $product;
    }

    /**
     * Return Product List in JSON
     * @return JSON
     */
    public function index()
    {
        $products = $this->product->paginate();

        return $products;
    }
}
