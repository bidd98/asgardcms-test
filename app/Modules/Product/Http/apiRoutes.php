<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' => '/product'], function(Router $router) {
    $router->get('products', [
        'as' => 'api.product.produc.index',
        'uses' => 'ProductController@index'
    ]);
});