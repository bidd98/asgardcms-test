<div class="box-body">
    <p>
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Name">
            {!! $errors->first("name", '<span style="color:red" class="help-block">:message</span>') !!}
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" name="price" class="form-control" id="price" placeholder="Price">
            {!! $errors->first("price", '<span style="color:red" class="help-block">:message</span>') !!}
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" rows="3"></textarea>
            {!! $errors->first("description", '<span style="color:red" class="help-block">:message</span>') !!}
        </div>

        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control-file" name="image" id="image">
            {!! $errors->first("image", '<span style="color:red" class="help-block">:message</span>') !!}
        </div>
    </p>
</div>